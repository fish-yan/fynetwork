# FYNetwork

[![CI Status](https://img.shields.io/travis/fish-yan/FYNetwork.svg?style=flat)](https://travis-ci.org/fish-yan/FYNetwork)
[![Version](https://img.shields.io/cocoapods/v/FYNetwork.svg?style=flat)](https://cocoapods.org/pods/FYNetwork)
[![License](https://img.shields.io/cocoapods/l/FYNetwork.svg?style=flat)](https://cocoapods.org/pods/FYNetwork)
[![Platform](https://img.shields.io/cocoapods/p/FYNetwork.svg?style=flat)](https://cocoapods.org/pods/FYNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FYNetwork is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FYNetwork'
```

## Author

fish-yan, 757094197@qq.com

## License

FYNetwork is available under the MIT license. See the LICENSE file for more info.
